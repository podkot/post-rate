<?php
/*
Plugin Name: Post Rate
Description: Ratings: clean, lightweight and easy
Authors: Scott Taylor, Сергей Ермаков
Version: 0.5
Author URI: http://tsunamiorigami.com
*/

$dir = dirname( __FILE__ );
include_once( $dir . '/php/functions.php' );
include_once( $dir . '/php/plugin.php' );

add_action( 'plugins_loaded', array( 'RatePlugin', 'get_instance' ) );
